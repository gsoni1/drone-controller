#include "Setup.h" 

#ifndef _RAWVALUES_H_
#define _RAWVALUES_H_

class RawValues {       
      // This class fetches the raw data from the IMU for the Acceleromter,
      // Gyroscope and Magnetometer. It is used throughout the program to 
      // increase readability for convenience. 
  public:             
      // Member functions include the X, Y and Z components of the Acceleromter,
      // Gyroscope and Magnetometer.
    float acc_x, acc_y, acc_z;       
    float gyro_x, gyro_y, gyro_z;
    float mag_x, mag_y;  
      // Function prototypes to labeling get commands
    void getAccX(float*);
    void getAccY(float*);
    void getAccZ(float*);
    void getGyrX(float*);
    void getGyrY(float*);
    void getGyrZ(float*);
    void getMagX(float*);
    void getMagY(float*);
  };

  void RawValues::getAccX(float* acc_x)
  { 
    // Requesting values from IMU and storing them in a variable
    ICM_20948_AGMT_t agmt = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0}};
    if(ICM_20948_get_agmt( &myICM, &agmt ) == ICM_20948_Stat_Ok){
      *acc_x = agmt.acc.axes.x;
    }
  }
  void RawValues::getAccY(float* acc_y)
  { 
    // Requesting values from IMU and storing them in a variable
    ICM_20948_AGMT_t agmt = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0}};
    if(ICM_20948_get_agmt( &myICM, &agmt ) == ICM_20948_Stat_Ok){
      *acc_y = agmt.acc.axes.y;
    }
  }
  void RawValues::getAccZ(float* acc_z)
  { 
    // Requesting values from IMU and storing them in a variable
    ICM_20948_AGMT_t agmt = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0}};
    if(ICM_20948_get_agmt( &myICM, &agmt ) == ICM_20948_Stat_Ok){
      *acc_z = agmt.acc.axes.z;
    }
  }
  void RawValues::getGyrX(float* gyro_x)
  { 
    // Requesting values from IMU and storing them in a variable
    ICM_20948_AGMT_t agmt = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0}};
    if(ICM_20948_get_agmt( &myICM, &agmt ) == ICM_20948_Stat_Ok){
      *gyro_x = agmt.gyr.axes.x;
    }
  }
  void RawValues::getGyrY(float* gyro_y)
  {
    // Requesting values from IMU and storing them in a variable
    ICM_20948_AGMT_t agmt = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0}};
    if(ICM_20948_get_agmt( &myICM, &agmt ) == ICM_20948_Stat_Ok){
      *gyro_y= agmt.gyr.axes.y;
    }
  }
  void RawValues::getGyrZ(float* gyro_z)
  {
    // Requesting values from IMU and storing them in a variable
    ICM_20948_AGMT_t agmt = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0}};
    if(ICM_20948_get_agmt( &myICM, &agmt ) == ICM_20948_Stat_Ok){
      *gyro_z = agmt.gyr.axes.z;
    }
  }
  void RawValues::getMagX(float* mag_x)
  {
    // Requesting values from IMU and storing them in a variable
    ICM_20948_AGMT_t agmt = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0}};
    if(ICM_20948_get_agmt( &myICM, &agmt ) == ICM_20948_Stat_Ok){
      *mag_x = agmt.mag.axes.x;
    }
  }
  void RawValues::getMagY(float* mag_y)
  {
    // Requesting values from IMU and storing them in a variable
    ICM_20948_AGMT_t agmt = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0}};
    if(ICM_20948_get_agmt( &myICM, &agmt ) == ICM_20948_Stat_Ok){
      *mag_y = agmt.mag.axes.y;
    }
  }
  
#endif // _RAWVALUES_H_
