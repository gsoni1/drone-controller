#include "ICM_20948.h"  
#include <Wire.h>

#ifndef _SETUP_H_
#define _SETUP_H_
#define ICM_I2C_ADDRESS 0x69 // This is the I2C address of the IMU

ICM_20948_Device_t myICM; // ICM_20948_Device_t class Instance, referenced throughout the program

// Preset reading and writing to registers in ICM
ICM_20948_Status_e my_write_i2c(uint8_t reg, uint8_t* data, uint32_t len, void* user);
ICM_20948_Status_e my_read_i2c(uint8_t reg, uint8_t* buff, uint32_t len, void* user);
const ICM_20948_Serif_t mySerif = {
  my_write_i2c, // write
  my_read_i2c,  // read
  NULL,         
};

ICM_20948_Status_e my_write_i2c(uint8_t reg, uint8_t* data, uint32_t len, void* user){
  Wire.beginTransmission(ICM_I2C_ADDRESS); 
  Wire.write(reg);
  Wire.write(data, len);
  Wire.endTransmission();
  return ICM_20948_Stat_Ok;
}

ICM_20948_Status_e my_read_i2c(uint8_t reg, uint8_t* buff, uint32_t len, void* user){
  Wire.beginTransmission(ICM_I2C_ADDRESS);
  Wire.write(reg);
  Wire.endTransmission(false); // Send repeated start
  
  uint32_t num_received = Wire.requestFrom(ICM_I2C_ADDRESS, len);
  if(num_received == len){
      for(uint32_t i = 0; i < len; i++){ 
          buff[i] = Wire.read();
      }
  }
  Wire.endTransmission();
  return ICM_20948_Stat_Ok;
}

unsigned long millisOld; // To retrieve (dt)
void setup() {
  Serial.begin(115200); // Baud rate
  Wire.begin();
  Wire.setClock(400000); //Clock Speed
  ICM_20948_link_serif(&myICM, &mySerif);
  ICM_20948_sw_reset( &myICM ); 
  delay(250);
  static uint32_t gyro_update_timer = 0;
  // Set Gyro and Accelerometer to a particular sample mode
  ICM_20948_set_sample_mode( &myICM, (ICM_20948_InternalSensorID_bm)(ICM_20948_Internal_Acc | ICM_20948_Internal_Gyr), ICM_20948_Sample_Mode_Continuous ); // optiona: ICM_20948_Sample_Mode_Continuous. ICM_20948_Sample_Mode_Cycled
  // Set full scale ranges for both acc and gyr
  ICM_20948_fss_t myfss;
  myfss.a = gpm2;   // (ICM_20948_ACCEL_CONFIG_FS_SEL_e) // Set Accelerometer to +-2g
  myfss.g = dps250; // (ICM_20948_GYRO_CONFIG_1_FS_SEL_e) // Set Gyroscope to +-250DPS
  ICM_20948_set_full_scale( &myICM, (ICM_20948_InternalSensorID_bm)(ICM_20948_Internal_Acc | ICM_20948_Internal_Gyr), myfss );
  ICM_20948_sleep         ( &myICM, false ); //This is to wake the sensor up
  ICM_20948_low_power     ( &myICM, false );
  millisOld = millis(); // To retrieve (dt)
}

#endif
