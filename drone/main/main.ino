// Gorav Soni
// Quadcopter Project
// CS 490 First Commit
// 11.12.20

#include <math.h> // Math Library
#include "Setup.h" // Initialization                
#include "RawValues.h" // For getting raw values 
#define GYRO_PART 0.980
#define ACCEL_PART 0.02

RawValues Sensor; // Instance of class RawValues from RawValues.h
float pitchM;
float rollM;

//GYRO
float gyro_anglesX = 0;
float gyro_anglesY = 0;
float dt; 
//unsigned long millisOld;
//COMBINE GYRO + ACCELERAMETER
float pitch=0;;
float roll=0;
// YAW
float mag_X1;
float mag_Y1;
float theta;

void loop() {
  float *gyro_angle_y;
  float *acc_angle_y;
  float *gyro_angle_x;
  float *acc_angle_x;
  float *delta_t;
  delay(10);
  process_accel(acc_angle_x,acc_angle_y);
  process_gyro(gyro_angle_y, gyro_angle_x, delta_t);
  combine();
}

void process_accel(float* acc_angle_x,float* acc_angle_y){
  // Outputs Pitch and Roll using only the Acceleromter values.
  // Advantage: Accurate values in the long run
  // Disadvantage: Acceleromter is susceptible to vibrations
  float acc_X, acc_Y, acc_Z;
  float X_out,Y_out,Z_out;

  // Retrieve values from sensor
  Sensor.getAccX(&acc_X); 
  Sensor.getAccY(&acc_Y);
  Sensor.getAccZ(&acc_Z);
  // Adjusting each component to the +-2g full scale value
  X_out = (getAccMG(acc_X,0)/1000);
  Y_out = (getAccMG(acc_Y,0)/1000);
  Z_out = (getAccMG(acc_Z,0)/1000 - 0.01);
  
  // Pitch using Accelerometer values
  pitchM=-atan2(X_out/9.8,Z_out/9.8)/2/PI*360; 
  // Roll using Accelerometer values
  rollM=-atan2(Y_out/9.8,Z_out/9.8)/2/PI*360;  
  *acc_angle_x = pitchM; 
  *acc_angle_y = rollM; 
}


void process_gyro(float*gyro_angle_y, float* gyro_angle_x, float* delta_t){
  // Advantage: Quick and snappy
  // Disadvantage: Vibrations, aswell as a Gyro Drift that causes data spread in the longterm  
  float gyr_X, gyr_Y;
  float X_out,Y_out,mag_x,mag_y;
  
  // Retrieve values from sensor
  Sensor.getGyrX(&gyr_X);
  Sensor.getGyrY(&gyr_Y);
  
  // Adjusting each component to +-250 DPS
  X_out = (getGyrDPS(gyr_X,0));
  Y_out = (getGyrDPS(gyr_Y,0));
  // Calculate dt for the integral 
  dt = (millis() - millisOld) / 1000.;
  millisOld = millis();
  // Summing up the rotations to find the angle 
  gyro_anglesX += Y_out * dt; 
  gyro_anglesY += X_out * dt;
  
  *gyro_angle_x = gyro_anglesX; 
  *gyro_angle_y = gyro_anglesY;
  *delta_t = dt;
}

void combine()
{
  // Combines the Pitch and Roll of the Gyroscope and Accelerometer
  // Outputs data that is quick/accurate with little to no vibrations 
  float gyr_Y, gyr_X, X_out, Y_out;
  float gyro_angleY,gyro_angleX, dtNEW;
  float accel_roll, accel_pitch;
  
  // Retrieve Gyroscope values from sensor
  Sensor.getGyrX(&gyr_X);
  Sensor.getGyrY(&gyr_Y);
  
  // Adjusting each component to +-250 DPS
  X_out = (getGyrDPS(gyr_X,0));
  Y_out = (getGyrDPS(gyr_Y,0));
  
  process_accel(&accel_pitch, &accel_roll);
  process_gyro(&gyro_angleX, &gyro_angleY, &dtNEW);
  
  // Recalculating Pitch and Roll by applying a filter
  // Taking 98% of the Gyroscope and 2% of the Accelerometer for Pitch and Roll
  pitch = (pitch+Y_out*dtNEW) * GYRO_PART + accel_pitch *ACCEL_PART;
  roll = (roll+X_out*dtNEW) * GYRO_PART + accel_roll * ACCEL_PART;
  Serial.print(pitch);
  Serial.print(',');
  //dt = 0.01; // dt adjustment(temporary)
  Serial.println(roll);  

}


float getAccMG( int16_t raw, uint8_t fss ){ // Function to set Accelerometer sensitivity 
  switch(fss){
    case 0 : return (((float)raw)/16.384); break;
    case 1 : return (((float)raw)/8.192); break;
    case 2 : return (((float)raw)/4.096); break;
    case 3 : return (((float)raw)/2.048); break;
    default : return 0; break;
  }
}

float getGyrDPS( int16_t raw, uint8_t fss ){ // Function to set Gyroscope sensitivity 
  switch(fss){
    case 0 : return (((float)raw)/131); break;
    case 1 : return (((float)raw)/65.5); break;
    case 2 : return (((float)raw)/32.8); break;
    case 3 : return (((float)raw)/16.4); break;
    default : return 0; break;
  }
}
